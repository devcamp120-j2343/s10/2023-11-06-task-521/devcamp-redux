import { Button, Container, Grid, List, ListItem, TextField } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { btnAddTaskClickAction, inputTaskChangeAction, toggleTaskClickAction } from "../actions/task.action";

const Tasks = () => {
    const dispatch = useDispatch();

    const { inputString, taskList } = useSelector((reduxData) => {
        return reduxData.taskReducer;
    });

    const inputChangeHandler = (event) => {
        dispatch(inputTaskChangeAction(event.target.value));
    }

    const btnAddTaskClicked = () => {
        dispatch(btnAddTaskClickAction());
    }

    const toggleTaskClicked = (i) => {
        dispatch(toggleTaskClickAction(i));
    }

    return (
        <Container>
            <Grid container spacing={2} mt={5} alignItems="center">
                <Grid item md={9}>
                    <TextField variant="standard" label="Input task name" fullWidth value={inputString} onChange={inputChangeHandler}/>
                </Grid>
                <Grid item md={3}>
                    <Button variant="contained" onClick={btnAddTaskClicked}>Add task</Button>
                </Grid>
            </Grid>
            <Grid container>
                <List>
                    {taskList.map((element, index) => {
                        return (
                            <ListItem 
                                key={index}
                                style={{color: element.status ? "green" : "red"}}
                                //Xử lý sự kiện xảy ra trong 1 list: Tạo 1 function gọi đến function có truyền thêm chỉ số
                                onClick={() => toggleTaskClicked(index)}
                            >
                                {index + 1}. {element.taskName}
                            </ListItem>
                        )
                    })}
                </List>
            </Grid>
        </Container>
    )
}

export default Tasks;


