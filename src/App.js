import Tasks from "./components/Tasks.component";

function App() {
  return (
    <div>
      <Tasks />
    </div>
  );
}

export default App;
