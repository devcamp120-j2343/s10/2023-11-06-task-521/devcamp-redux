import { BUTTON_ADD_TASK_CLICKED, INPUT_TASK_CHANGE, TASK_TOGGLE_CLICKED } from "../constants/task.constants"

export const inputTaskChangeAction = (inputValue) => {
    return {
        type: INPUT_TASK_CHANGE,
        payload: inputValue
    }
}

export const btnAddTaskClickAction = () => {
    return {
        type: BUTTON_ADD_TASK_CLICKED
    }
}

export const toggleTaskClickAction = (index) => {
    return {
        type: TASK_TOGGLE_CLICKED,
        payload: index
    }
}