import { BUTTON_ADD_TASK_CLICKED, INPUT_TASK_CHANGE, TASK_TOGGLE_CLICKED } from "../constants/task.constants";

const initialState = {
    inputString: "",
    taskList: []
}

// Khai báo task reducer
const taskReducer = (state = initialState, action) => {
    switch (action.type) {
        case INPUT_TASK_CHANGE:
            state.inputString = action.payload;
            break;
        case BUTTON_ADD_TASK_CLICKED:
            state.taskList = [...state.taskList, {
                taskName: state.inputString,
                status: false
            }];

            state.inputString = "";
            break;
        case TASK_TOGGLE_CLICKED:
            state.taskList[action.payload].status = ! state.taskList[action.payload].status;
            break;
        default:
            break;
    }

    return {...state};
}

export default taskReducer;