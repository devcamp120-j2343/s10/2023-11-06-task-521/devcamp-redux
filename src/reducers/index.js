// Root reducer
import { combineReducers } from "redux";
import taskReducer from "./task.reducer";

// Tạo root reducer
const rootReducer = combineReducers({
    taskReducer
});

export default rootReducer;